BEFORE OPENNIG THE PROJECT :

1. Enter "vcpkg integrate install" in the cmd in '/vcpkg' path*.
2. Set the arch to x86.
3. Restart your solution (if it's open).



* You should get the following message:

	Applied user-wide integration for this vcpkg root.

	All MSBuild C++ projects can now #include any installed libraries.
	Linking will be handled automatically.
	Installing new libraries will make them instantly available.

	CMake projects should use: "-DCMAKE_TOOLCHAIN_FILE=C:/.../trivia-project/vcpkg/scripts/buildsystems/vcpkg.cmake"
boost-assert f0164d81d34d5445011c42ba83338e0a994480de
boost-compatibility b8c066dd12592d501582c3fa90d40696203b78d9
boost-config 20cb5b4d1e7456b3d2a769884917011c84bf3510
boost-core 5b621ead1d346332e9fb85a7c3d9a427a42dd19e
boost-detail 6f4d9b93ca309167fb6dc5f70a1f8ffd5da2442f
boost-optional 05f0fbf3d961680d3beeceaae32af5968d3a62f0
boost-smart-ptr 383b9a35fd0bfb02b53c2e9418ab8d65a1425fcf
boost-throw-exception 4d2d23bdb9e2ecb9531ea2c0c487af666626d37f
boost-utility b6ff9991bc3dcf6748fc4136c4ad1c6d993d27d4
boost-vcpkg-helpers 2d54b6d10f1dc434d4c1ffdf6ffecb250df51316
cmake 3.20.2
features core
portfile.cmake 488a1c186b969ab1e8e0dfa4b915c9106e023e10
ports.cmake bd8d6584852d3c42c455b6794fc31eb6a4f0914a
post_build_checks 2
powershell 7.1.3
triplet x86-windows
triplet_abi 4c64db251cbf53c76a606595b52416ec991ab5bb-a1c0eabb0c5177b6a8fb97a58ae398880c47b352-d8c1982fc772c9b47c816273e96145953dc4d7ea
vcpkg.json 4c4475e963446609ca59b198231c7d66ff08e973
vcpkg_from_git 99e83c6ffac2dd07afaf73d795988ca5dec2dc8b
vcpkg_from_github a9dd1453b8873c9702731c6c36e79fb0ab6ae486
